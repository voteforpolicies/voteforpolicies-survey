import express, * as Express from 'express'
import appV1, * as AppV1 from './v1'

export interface Configuration {
    cors?: string[]
    cacheType?: string
}

const makeApp = ({ cacheType = 'file', cors = [] }: Configuration = {}): Express.Application => {
  const app = express()

  // Handle CORS
  if (cors.length > 0) {
    // for ease of use, so "".split(',') is valid
    const nonEmptyCors = cors.filter(origin => origin !== '')

    nonEmptyCors.forEach(origin => {
      try {
        // Node APIs do not provide a compliant alternative
        // eslint-disable-next-line no-new
        new URL(origin)
      } catch {
        throw new Error('Invalid URL for configured cors origin: ' + origin)
      }
    })

    app.use(function (req: Express.Request, res: Express.Response, next) {
      if (req.headers.origin !== undefined && nonEmptyCors.includes(req.headers.origin)) {
        res.header('Access-Control-Allow-Origin', req.headers.origin)
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
      }
      next()
    })
  }

  let cacheModule
  try {
    cacheModule = require('./cache/' + cacheType)
  } catch (err) {
    throw new Error('Invalid Cache type: ' + cacheType)
  }

  // Initialise and attach the API versions
  AppV1.init(cacheModule)
  app.use('/api/v1', appV1)

  // Fallback for routes not found (404)
  app.use((req: Express.Request, res: Express.Response) => {
    return res.status(404).send()
  })

  return app
}

export default makeApp
