import app, * as App from '../v1'
import * as Cache from '../cache/file'
import request from 'supertest'

App.init(Cache)

describe('Test the current survey endpoint', () => {
  test('Ensure we are responding type json', async () => {
    const response = await request(app).get('/survey/current')
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
  })
})

describe('Test the submit survey endpoint', () => {
  test('Ensure we are responding type json', async () => {
    const response = await request(app).post('/survey/submit')
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
  })
})
