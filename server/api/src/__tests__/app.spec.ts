import makeApp from '../app'
import request from 'supertest'

import * as ApiTypes from 'vfp-survey-lib/types'

interface PostDataType {
  selections: ApiTypes.Selection[]
  filters: ApiTypes.Filter[]
}

jest.spyOn(console, 'log').mockImplementation()

describe('Test dynamic cache module loading', () => {
  test('Exceptions if invalid type', async () => {
    expect(() => {makeApp({ cacheType: 'invalid' })}).toThrow('Invalid Cache type: invalid')
  })

  test('Defaults to file type', async () => {
    const app = makeApp({})
    const response = await request(app).get('/api/v1/survey/current')
    expect(response.text).toContain('"id":1,"name":"England"')
  })

  test('Can use file type', async () => {
    const app = makeApp({ cacheType: 'file' })
    const response = await request(app).get('/api/v1/survey/current')
    expect(response.text).toContain('"id":2,"name":"N. Ireland"')
  })
})

describe('Test the app handles CORS correctly', () => {
  test('handles no config by not enabling CORS', async () => {
    const app = makeApp({ cors: [] })
    const response = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
    expect(response.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(response.headers['Access-Control-Allow-Headers']).toBeUndefined()
  })

  test('handles config without cors by not enabling CORS', async () => {
    const app = makeApp({ cors: [] })
    const response = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
    expect(response.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(response.headers['Access-Control-Allow-Headers']).toBeUndefined()
  })

  test('handles explicit empty cors list by not enabling CORS', async () => {
    const app = makeApp({ cors: [] })
    const response = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
    expect(response.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(response.headers['Access-Control-Allow-Headers']).toBeUndefined()
  })

  test.each([
    ['not a url'],
    ['http://example.com', 'not a url'],
    ['', 'not a url'],
  ])('handles non-empty invalid urls given as cors configuration by throwing an error', (...cors: string[]) => {
    expect(() => { makeApp({ cors }) }).toThrowError()
  })

  test('handles config with one empty url by ignoring it', async () => {
    const app = makeApp({ cors: [''] })
    const responseEmptyOrigin = await request(app).get('/').set({ origin: '' })
    expect(responseEmptyOrigin.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(responseEmptyOrigin.headers['Access-Control-Allow-Headers']).toBeUndefined()

    const responseNonEmptyOrigin = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
    expect(responseNonEmptyOrigin.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(responseNonEmptyOrigin.headers['Access-Control-Allow-Headers']).toBeUndefined()
  })

  test('handles config with empty urls by ignoring them', async () => {
    const app = makeApp({ cors: ['', '', 'https://voteforpolicies.org.uk', ''] })

    const responseEmptyOrigin = await request(app).get('/').set({ origin: '' })
    expect(responseEmptyOrigin.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(responseEmptyOrigin.headers['Access-Control-Allow-Headers']).toBeUndefined()

    const responseVfpOrigin = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
    expect(responseVfpOrigin.headers['access-control-allow-origin']).toBe('https://voteforpolicies.org.uk')
    expect(responseVfpOrigin.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept')

    const responseWrongOrigin = await request(app).get('/').set({ origin: 'https://notvoteforpolicies.org.uk' })
    expect(responseWrongOrigin.headers['Access-Control-Allow-Origin']).toBeUndefined()
    expect(responseWrongOrigin.headers['Access-Control-Allow-Headers']).toBeUndefined()
  })

  describe('handles singleton valid url', () => {
    const app = makeApp({ cors: ['https://voteforpolicies.org.uk'] })

    test('requested with correct origin header', async () => {
      const response = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
      expect(response.headers['access-control-allow-origin']).toBe('https://voteforpolicies.org.uk')
      expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept')
    })

    test('requested with correct Origin header', async () => {
      const response = await request(app).get('/').set({ Origin: 'https://voteforpolicies.org.uk' })
      expect(response.headers['access-control-allow-origin']).toBe('https://voteforpolicies.org.uk')
      expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept')
    })

    test('requested with incorrect origin header', async () => {
      const response = await request(app).get('/').set({ origin: 'https://notvoteforpolicies.org.uk' })
      expect(response.headers['access-control-allow-origin']).toBeUndefined()
      expect(response.headers['access-control-allow-headers']).toBeUndefined()
    })

    test('requested with an invalid origin header', async () => {
      const response = await request(app).get('/').set({ origin: 'not a url' })
      expect(response.headers['access-control-allow-origin']).toBeUndefined()
      expect(response.headers['access-control-allow-headers']).toBeUndefined()
    })

    test('requested without an origin header', async () => {
      const response = await request(app).get('/')
      expect(response.headers['access-control-allow-origin']).toBeUndefined()
      expect(response.headers['access-control-allow-headers']).toBeUndefined()
    })
  })

  describe('handles multiple valid urls', () => {
    const app = makeApp({ cors: ['https://voteforpolicies.org.uk', 'https://example.com'] })

    test('requested with correct origin header [0]', async () => {
      const response = await request(app).get('/').set({ origin: 'https://voteforpolicies.org.uk' })
      expect(response.headers['access-control-allow-origin']).toBe('https://voteforpolicies.org.uk')
      expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept')
    })

    test('requested with correct origin header [1]', async () => {
      const response = await request(app).get('/').set({ origin: 'https://example.com' })
      expect(response.headers['access-control-allow-origin']).toBe('https://example.com')
      expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept')
    })

    test('requested with incorrect origin header', async () => {
      const response = await request(app).get('/').set({ origin: 'https://notvoteforpolicies.org.uk' })
      expect(response.headers['access-control-allow-origin']).toBeUndefined()
      expect(response.headers['access-control-allow-headers']).toBeUndefined()
    })

    test('requested with an invalid origin header', async () => {
      const response = await request(app).get('/').set({ origin: 'not a url' })
      expect(response.headers['access-control-allow-origin']).toBeUndefined()
      expect(response.headers['access-control-allow-headers']).toBeUndefined()
    })

    test('requested without an origin header', async () => {
      const response = await request(app).get('/')
      expect(response.headers['access-control-allow-origin']).toBeUndefined()
      expect(response.headers['access-control-allow-headers']).toBeUndefined()
    })
  })
})

describe('Test the app serves the V1 API', () => {
  const app = makeApp()

  test.each(['/api/v1/survey/current'])('The app serves %s', async (path) => {
    const response = await request(app).get(path)
    expect(response.statusCode).not.toBe(404)
    expect(response.statusCode).toBeLessThan(500)
  })
})

describe('Test the submit survey endpoint', () => {
  const app = makeApp()

  test('both errors called if no post data', async () => {
    const response = await request(app).post('/api/v1/survey/submit')
    expect(console.log).toHaveBeenCalledWith('Postcode filter missing from body: ', {})
    expect(console.log).toHaveBeenCalledWith('Selections missing from body: ', {})
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('invalid postcode response if no post data', async () => {
    const body = {
      selections: [],
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Postcode filter missing from body: ', body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('invalid postcode response if invalid post data', async () => {
    const body: PostDataType = {
      selections: [],
      filters: [] // The server assumes one member of the array
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Postcode filter missing from body: ', body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('invalid postcode response if non string postcode data', async () => {
    const body = {
      selections: [],
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('invalid selection response if no selection data', async () => {
    const body = {
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Selections missing from body: ', body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('\"selections\":[]')
  })

  test('invalid selection response if string post data', async () => {
    const body = {
      selections: "not an array",
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Selections not array type: ', body.selections)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('\"selections\":[]')
  })

  test('invalid selection response if invalid array data in post', async () => {
    const body = {
      selections: ["one", 2],
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Invalid selections in body: ', body.selections)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('\"selections\":[]')
  })

  test('invalid selection response if array of invalid objects in post data', async () => {
    const body = {
      selections: [
        {
          one: 1,
          two: 2
        },
        {
          three: 3,
          four: 4
        }
      ],
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Invalid selections in body: ', body.selections)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('\"selections\":[]')
  })

  test('no invalid selection response if array is empty in post data', async () => {
    const body = {
      selections: [],
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(console.log).not.toHaveBeenLastCalledWith('Selections missing from body: ', body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('\"selections\":[]')
  })

  test('selections to be included in response', async () => {
    const selections = [
      {issueId: 0, partyId: 4},
      {issueId: 1, partyId: 2},
      {issueId: 2, partyId: 1},
    ]
    const body = {
      selections: selections,
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain(JSON.stringify(selections))
  })

  test('Ensure we are responding type json', async () => {
    const body: PostDataType = {
      selections: [],
      filters: [{
        division: 2,
        value: 'AA'
      }]
    }

    const response = await request(app).post('/api/v1/survey/submit').send(body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).not.toContain('Postcode not found')
  })
})

describe('Test the get individual results endpoint', () => {
  const app = makeApp()

  test('Failing to provide an id returns not found', async () => {
    const response = await request(app).get('/api/v1/survey/results/')
    expect(response.statusCode).toBe(404)
  })

  test('Providing an invalid id returns not found', async () => {
    const response = await request(app).get('/api/v1/survey/results/not-going-to-exist')
    expect(response.statusCode).toBe(404)
    expect(response.text).toBe('{\"message\":\"Invalid results id\"}')
  })

  test('Ensure we can get back a recently submitted results set', async () => {
    const selections = [
      {issueId: 0, partyId: 4},
      {issueId: 1, partyId: 2},
      {issueId: 2, partyId: 1},
    ]

    const body: PostDataType = {
      selections: selections,
      filters: [{
        division: 2,
        value: 'AA'
      }]
    }

    const submitResponse = await request(app).post('/api/v1/survey/submit').send(body)
    expect(submitResponse.statusCode).toBe(200)

    const results = JSON.parse(submitResponse.text)

    const fetchResponse = await request(app).get(`/api/v1/survey/results/${results.resultsId}`)
    expect(fetchResponse.statusCode).toBe(200)
    expect(fetchResponse.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(fetchResponse.text).toContain(JSON.stringify(selections))
    expect(fetchResponse.text).toContain('\"parties\":')
    expect(fetchResponse.text).toContain('\"results\":')
  })

})

test('the app serves a 404 for missing routes', async () => {
  const app = makeApp()
  const response = await request(app).get('/missing')
  expect(response.statusCode).toBe(404)
})
