import * as ApiTypes from 'vfp-survey-lib/types'

export interface Cache {
  init: () => void
  getSurveyData: (surveyId: number) => Promise<any>
  getCandidateList: (surveyId: number) => Promise<ApiTypes.Party[]>
  getConstituencyData: (surveyId: number, constituencyId: number)
      => Promise<ApiTypes.Constituency>
  getDivisionResults: (surveyId: number, divisionLvl: number, divisionId: number)
      => Promise<ApiTypes.Result>
  getIndividualResults: (surveyId: number, resultsId: string) => Promise<{
    constituencyId: number, selections: ApiTypes.Selection[]
  }>
  getLookup: (surveyId: number, lookupRef: string) => Promise<number | null>
  putIndividualResults: (surveyId: number, resultsId: string, constituencyId: number,
                         selections: ApiTypes.Selection[] ) => Promise<boolean>
}
