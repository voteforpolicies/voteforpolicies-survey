/*
  File Cache

  A data cache based on files.

  It is only meant as a baby step to a real cache system. In cloud hosting
  we don't intend to store or amend files therefore it is a read only system.
*/

import * as ApiTypes from 'vfp-survey-lib/types'
import * as current from './data/current.json'
import * as lookup from './data/lookup.json'
import * as results from './data/results.json'

let store: {survey: any}

export function init () {
  store = {
    survey: {
      0: {
        data: current,
        candidates: results.parties,
        lookup: lookup.lookup,
        constituencies: results.constituencies,
        divisionResults: results.results,
        individualResults: {},
      }
    }
  }
}

function get (surveyId: number, lookups: string[]):
  Promise<any | ApiTypes.Party[] | ApiTypes.Result | ApiTypes.Constituency | ApiTypes.Country | number> {
  return new Promise((resolve, reject) => {
    let ptr = store.survey[surveyId]
    if (typeof ptr === 'undefined') {
      reject(new Error(`Invalid survey id: ${surveyId}`))
    }

    let level = 0
    lookups.forEach(l => {
      ptr = ptr[l]
      level++
      if (typeof ptr === 'undefined') {
        reject(new Error(`Cannot find ${l} at ${level} level`))
      }
    })

    resolve(ptr)
  })
}

function put (surveyId: number, lookups: string[], key: string, value: any): Promise<boolean> {
  return new Promise((resolve, reject) => {
    let ptr = store.survey[surveyId]
    if (typeof ptr === 'undefined') {
      reject(new Error(`Invalid survey id: ${surveyId}`))
    }

    let level = 0
    lookups.forEach(l => {
      ptr = ptr[l]
      level++
      if (typeof ptr === 'undefined') {
        reject(new Error(`Cannot find ${l} at ${level} level`))
      }
    })

    if (typeof ptr[key] !== 'undefined') {
      reject(new Error(`Duplicate key: ${key}`))
    }

    ptr[key] = value

    resolve(true)
  })
}

export function getSurveyData (surveyId: number): Promise<any> {
  return get(surveyId, ['data'])
}

export function getCandidateList (surveyId: number): Promise<ApiTypes.Party[]> {
  return get(surveyId, ['candidates'])
}

export function getDivisionResults (surveyId: number, divisionLvl: number, divisionId: number):
  Promise<ApiTypes.Result> {
  return new Promise((resolve, reject) => {
    get(surveyId, ['divisionResults', divisionLvl.toString(), divisionId.toString()])
      .then(res => {
        return resolve(res)
      })
      .catch(err => {
        if (err.message.endsWith('3 level')) {
          return reject(new Error('Invalid division'))
        } else if (err.message.endsWith('2 level')) {
          return reject(new Error('Invalid division level'))
        }
        return reject(err)
      })
  })
}

export function getConstituencyData (surveyId: number, constituencyId: number):
  Promise<ApiTypes.Constituency> {
  return new Promise((resolve, reject) => {
    get(surveyId, ['constituencies', constituencyId.toString()])
      .then(res => {
        return resolve(res)
      })
      .catch(err => {
        if (err.message.endsWith('2 level')) {
          return reject(new Error('Invalid constituency id'))
        }
        return reject(err)
      })
  })
}

export function getLookup (surveyId: number, lookupRef: string): Promise<number | null> {
  return new Promise((resolve, reject) => {
    get(surveyId, ['lookup', lookupRef])
      .then(res => {
        return resolve(res)
      })
      .catch(err => {
        if (err.message.startsWith('Cannot find')) {
          return resolve(null)
        }
        return reject(err)
      })
  })
}

export function getIndividualResults (surveyId: number, resultsId: string):
  Promise<{constituencyId: number, selections: ApiTypes.Selection[]}>
{
  return new Promise((resolve, reject) => {
    get(surveyId, ['individualResults', resultsId])
      .then(res => {
        return resolve(res)
      })
      .catch(err => {
        if (err.message.endsWith('2 level')) {
          return reject(new Error('Invalid results id'))
        }
        return reject(err)
      })
  })
}

export function putIndividualResults (
  surveyId: number, resultsId: string, constituencyId: number,
  selections: ApiTypes.Selection[] ): Promise<boolean>
{
  return new Promise((resolve, reject) => {
    const data = {constituencyId: constituencyId, selections: selections}
    put(surveyId, ['individualResults'], resultsId, data)
      .then(res => {
        return resolve(res)
      })
      .catch(err => {
        return reject(err)
      })
  })
}
