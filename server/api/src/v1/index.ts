import express, * as Express from 'express'
import short from 'short-uuid'
import { Cache } from '../cache/types'
import * as ApiTypes from 'vfp-survey-lib/types'

type NonPersonalResults = {
  constituency: ApiTypes.Constituency
  parties: ApiTypes.Party[]
  results: {
    constituency: ApiTypes.Result
    country: ApiTypes.Result
    nation: ApiTypes.Result
  }
}

const app: Express.Application = express()
app.use(express.json())

let cache: Cache

export function init (cacheModule: Cache) {
  cache = cacheModule
  cache.init()
}

app.get('/survey/results/:id', async (req: Express.Request, res: Express.Response) => {
  const surveyId = 0
  const resultsId = req.params.id

  try {
    const indRes = await cache.getIndividualResults(surveyId, resultsId)
    const npRes = await fetchNonPersonalResults(surveyId, indRes.constituencyId)
    return res.send({...indRes, ...npRes})
  } catch (err) {
    return res.status(404).send({message: "Invalid results id"})
  }
})

app.post('/survey/submit', async (req: Express.Request, res: Express.Response) => {
  const postcode = extractPostcode(req)
  const selections = extractSelections(req)
  const surveyId = 0

  const constituencyLookup = await cache.getLookup(surveyId, postcode)
  const constituencyId = constituencyLookup === null ? -1 : constituencyLookup

  const resultsId = String(short.generate())

  const fetchstore = await Promise.all([
    fetchNonPersonalResults(surveyId, constituencyId),
    cache.putIndividualResults(surveyId, resultsId, constituencyId, selections)
  ])

  const indiviualResponse = {
    resultsId: fetchstore[1] ? resultsId : '',
    selections: selections
  }

  res.send({...indiviualResponse, ...fetchstore[0]})
})

app.get('/survey/current', async (req: Express.Request, res: Express.Response) => {
  const surveyId = 0

  cache.getSurveyData(surveyId)
    .then(surveyData => {
      return res.send(surveyData)
    })
    .catch(err => {
      console.log(err)
      return res.sendStatus(500)
    })
})

function extractPostcode(req: Express.Request): string {
  try {
    return String(req.body.filters[0].value)
  } catch (err) {
    console.log('Postcode filter missing from body: ', req.body)
    return 'NOTFOUND'
  }
}

function extractSelections(req: Express.Request): ApiTypes.Selection[] {
  if (req.body.selections === undefined) {
    console.log('Selections missing from body: ', req.body)
    return []
  }

  const unclean = req.body.selections as any[]
  if (unclean.constructor !== Array ) {
    console.log('Selections not array type: ', unclean)
    return []
  }

  const selections: ApiTypes.Selection[] = []
  unclean.forEach( s => {
    if (s.issueId !== undefined && typeof s.issueId  === 'number' &&
        s.partyId !== undefined && typeof s.partyId  === 'number') {
      selections.push({issueId: s.issueId, partyId: s.partyId})
    }
  })

  if (selections.length != unclean.length) {
    console.log('Invalid selections in body: ', unclean)
  }
  
  return selections
}

async function fetchNonPersonalResults(surveyId: number, constituencyId: number): Promise<NonPersonalResults> {
  let constituencyData: ApiTypes.Constituency
  let constituencyResults: ApiTypes.Result
  let countryResults: ApiTypes.Result

  if (constituencyId === -1) {
    constituencyData = {
      id: -1,
      name: 'Postcode not found',
      countryId: -1,
    }
    constituencyResults = {
      surveyCount: 0,
      results: [],
    }
    countryResults = constituencyResults
  } else {
    const constituencyLookups = await Promise.all([
      cache.getConstituencyData(surveyId, constituencyId),
      cache.getDivisionResults(surveyId, 2, constituencyId)
    ])
    constituencyData = constituencyLookups[0]
    constituencyResults = constituencyLookups[1]
    countryResults = await cache.getDivisionResults(0, 1, constituencyData.countryId)
  }

  const cacheLookups = await Promise.all([
    cache.getCandidateList(surveyId),
    cache.getDivisionResults(surveyId, 0, 0)
  ])

  return {
    constituency: constituencyData,
    parties: cacheLookups[0],
    results: {
      constituency: constituencyResults,
      country: countryResults,
      nation: cacheLookups[1]
    }
  }
}

export default app
