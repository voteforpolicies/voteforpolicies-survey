import * as Express from 'express'
import makeApp from './app'
import 'dotenv/config'

const port: string = process.env.PORT ?? '5000'
const protocol: string = process.env.PROTOCOL ?? 'http'
const host: string = process.env.HOST ?? 'localhost'
const cors: string[] = (process.env.CORS_ORIGINS ?? '').split(',')

const app: Express.Application = makeApp({
  ...(process.env.CACHE_TYPE && {cacheType: process.env.CACHE_TYPE}),
  cors
})

app.listen(port, () => {
  console.log(`Listening at ${protocol}://${host}:${port}`)
})
