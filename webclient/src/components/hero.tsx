import React from 'react'
import classes from './hero.module.scss'
import t from '../translations'

const Hero = () => (
  <div className={classes.hero}>
    <div className={classes.constrain}>
      <h1 className={classes.heroH1}>{t('heroH1')}</h1>
      <h2 className={classes.heroH2}>
        <strong>{t('heroH2Main')}</strong>
        <br />
        {t('heroH2Sub')}
      </h2>
    </div>
  </div>
)

export default Hero
