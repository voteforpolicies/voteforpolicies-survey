import React from 'react'
import classes from './surveyInfo.module.scss'
import classnames from 'classnames'

type Props = {
  children: React.ReactElement | string
  error?: boolean
}

const SurveyInfo = ({ children, error = false }: Props) => {
  return (
    <div className={classes.center}>
      <span
        className={classnames(classes.status, { [classes.error]: error })}
        // Role switch triggers screen readers to read out the prompt when it is an error
        role={error ? 'alert' : 'note'}
      >
        <i className={classes.icon} />
        {children}
      </span>
    </div>
  )
}

export default SurveyInfo
