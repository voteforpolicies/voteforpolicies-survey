import React, { useState } from 'react'
import SurveyButton from './surveyButton'
import t from '../translations'
import classes from './postcodeInput.module.scss'

type Props = {
  onComplete: (postcode: string) => void
}

const PostcodeInput = ({ onComplete }: Props) => {
  const [submitted, setSubmitted] = useState(false)
  const [postcode, setPostcode] = useState<string>('')

  const handleButtonClick = () => {
    if (postcode.length > 0) {
      setSubmitted(true)
      onComplete(postcode)
    }
  }

  const handlePostcodeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPostcode(event.target.value)
  }

  return (
    <div className={classes.constrain}>
      <div className={classes.constrainInner}>
        <div className={classes.content}>
          <fieldset>
            <legend className={classes.titleContainer}>
              <span className={classes.header}>{t('postcodeEnter')}</span>
              <p className={classes.subHeader}>{t('postcodeEntryLabel')}</p>
            </legend>
            <div className={classes.postcodeSection}>
              <div className={classes.postcode}>
                <label>
                  {t('postcodeEnter')}
                  <input
                    className={classes.input}
                    type='text'
                    placeholder={t('postcodeEnter')}
                    onChange={handlePostcodeInput}
                  />
                </label>
                {submitted ? (
                  t('surveySubmitted')
                ) : (
                  <div className={classes.btnContainer}>
                    <SurveyButton
                      icon
                      onClick={handleButtonClick}
                      buttonLabel={t('surveySubmit')}
                    />
                  </div>
                )}
              </div>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
  )
}

export default PostcodeInput
