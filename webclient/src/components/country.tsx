import React from 'react'
import classes from './country.module.scss'
import classnames from 'classnames'

type Props = {
  checked: boolean
  country: { id: number; name: string }
  onClick: (countryId: number) => void
}

const Country = ({ checked, country, onClick }: Props) => {
  const id = country.id?.toString() || 'unselected'
  const countryClass =
    classes[country.name.toLowerCase() as keyof typeof classes]
  return (
    <li className={classes.country}>
      <div>
        <input
          checked={checked}
          id={id}
          onChange={() => onClick(country.id)}
          onClick={() => checked && onClick(country.id)}
          type='radio'
          className={classes.countryInput}
        />
        <label
          className={classnames(countryClass, { [classes.selected]: checked })}
          htmlFor={id}
        >
          {country.name}
        </label>
      </div>
    </li>
  )
}

export default Country
