import React from 'react'
import SurveyInfo from './surveyInfo'
import t from '../translations'
import { Trans } from 'react-i18next'

type Props = {
  numSelected: number
  error?: boolean
}

const IssueSelectionStatus = ({ numSelected, error = false }: Props) => {
  const getDisplayMessage = () => {
    if (numSelected > 0) {
      const from = numSelected * 2
      const to = numSelected * 3
      return (
        <Trans i18nKey='surveyTimeEstimateTemplate' count={numSelected}>
          With <strong>{{ numSelected }} issue</strong> selected the survey will
          take approx.{' '}
          <strong>
            {{ from }}-{{ to }} min
          </strong>
        </Trans>
      )
    } else {
      return <span>{' ' + t('selectAtLeastOneIssue')}</span>
    }
  }

  return <SurveyInfo error={error}>{getDisplayMessage()}</SurveyInfo>
}

export default IssueSelectionStatus
