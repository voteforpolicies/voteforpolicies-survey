import classnames from 'classnames'
import React from 'react'
import classes from './surveyButton.module.scss'

type Props = {
  buttonLabel: string
  className?: string
  icon?: boolean
  onClick: () => void
  onMouseDown?: () => void
  type?: 'positive' | 'negative'
  disabled?: boolean
}

const SurveyButton = ({
  buttonLabel,
  className,
  icon = false,
  onClick,
  onMouseDown,
  type = 'positive',
  disabled = false,
}: Props) => {
  return (
    <div className={className}>
      <button
        className={classnames(classes.surveyButton, classes[type])}
        onClick={onClick}
        onMouseDown={onMouseDown}
        aria-disabled={disabled}
      >
        <span>{buttonLabel}</span>
        {icon && <i className={classes.surveyButtonIcon}></i>}
      </button>
    </div>
  )
}

export default SurveyButton
