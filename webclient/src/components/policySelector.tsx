import React, { useState } from 'react'
import FinalPolicyChoice from './finalPolicyChoice'
import ProgressMarkers from './progressMarkers'
import SinglePolicyChoice from './singlePolicyChoice'
import t from '../translations'
import * as ApiTypes from 'vfp-survey-lib/types'
import classes from './policySelector.module.scss'

type TextEmbeddedUrlType = {
  start: string
  linkText: string
  linkUrl: string
  end: string
}

type Props = {
  country: ApiTypes.Country
  issues: ApiTypes.Issue[]
  issuesSelected: number[]
  onComplete: (policies: number[]) => void
  policySets: ApiTypes.PolicySet[]
  onDecision: () => void
}

/* Randomize array in-place using Durstenfeld shuffle algorithm */
const shuffleArray = (array: unknown[]) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[array[i], array[j]] = [array[j], array[i]]
  }
}

const PolicySelector = ({
  country,
  issues,
  issuesSelected,
  onComplete,
  policySets,
  onDecision,
}: Props) => {
  // TODO no constructor now... investigate!
  // Cannot use state as it is called in the constructor
  const findPolicySetIndex = (issueIndex: number) => {
    const issueId = issuesSelected[issueIndex]
    return policySets.findIndex(i => i.issueId === issueId)
  }
  // TODO no constructor now... investigate!
  // Cannot use state as it is called in the constructor
  const parties = country.parties ?? []
  const shuffledPolicyList = (policySetIndex: number) => {
    const policyIndexes = policySets[policySetIndex].partyPolicies.map((p, i) =>
      parties.includes(p.partyId) ? i : -1
    )
    const cleanPolicyIndexes = policyIndexes.filter(p => p !== -1)
    shuffleArray(cleanPolicyIndexes)
    return cleanPolicyIndexes
  }

  const [finalChoice, setFinalChoice] = useState<boolean>(false)
  const [issueSelectedIndex, setIssueSelectedIndex] = useState<number>(0)
  const [maybePile, setMaybePile] = useState<number[]>([])
  const [partyPolicyIndex, setPartyPolicyIndex] = useState<number>(0)
  const [policySetIndex, setPolicySetIndex] = useState<number>(
    findPolicySetIndex(0)
  )
  const [selectedPolicyIds, setSelectedPolicyIds] = useState<number[]>([])
  const [shuffledPolicySetIndexes, setShuffledPolicySetIndexes] = useState<
    number[]
  >(shuffledPolicyList(0))

  const initialSelection = (maybe: boolean) => {
    const newPartyPolicyIndex = partyPolicyIndex + 1
    const issueComplete = newPartyPolicyIndex >= shuffledPolicySetIndexes.length

    if (issueComplete) {
      setFinalChoice(true)
    } else {
      setPartyPolicyIndex(newPartyPolicyIndex)
    }

    if (maybe) {
      setMaybePile([...maybePile, partyPolicyIndex])
    }

    onDecision()
  }

  const finalSelection = (policyIndex: number) => {
    const newIssueSelectedIndex = issueSelectedIndex + 1
    if (newIssueSelectedIndex >= issuesSelected.length) {
      onComplete([...selectedPolicyIds, policyIndex])
    } else {
      const policySetIndex = findPolicySetIndex(newIssueSelectedIndex)

      setFinalChoice(false)
      setIssueSelectedIndex(newIssueSelectedIndex)
      setMaybePile([])
      setPolicySetIndex(policySetIndex)
      setPartyPolicyIndex(0)
      setSelectedPolicyIds([...selectedPolicyIds, policyIndex])
      setShuffledPolicySetIndexes(shuffledPolicyList(policySetIndex))
    }
  }

  const handleNoThanks = () => {
    initialSelection(false)
  }

  const handleMaybe = () => {
    initialSelection(true)
  }

  const getFinalChoice = () => {
    let parts: TextEmbeddedUrlType
    if (maybePile.length === 0) {
      parts = {
        start: t('policySelectNoMaybe.start'),
        linkText: t('policySelectNoMaybe.linkText'),
        linkUrl: t('policySelectNoMaybe.linkUrl'),
        end: t('policySelectNoMaybe.end'),
      }
    } else {
      parts = {
        start: t('policySelectShortlist.start'),
        linkText: t('policySelectShortlist.linkText'),
        linkUrl: t('policySelectShortlist.linkUrl'),
        end: t('policySelectShortlist.end'),
      }
    }
    const maybeIndexes: number[] = !maybePile.length
      ? [...shuffledPolicySetIndexes].sort((a, b) => a - b)
      : maybePile
    return (
      <>
        <div className={classes.divide}>
          <div className={classes.constrain}>
            <p className={classes.intro}>
              {parts.start}
              <a href={parts.linkUrl}>{parts.linkText}</a>
              {parts.end}
            </p>
          </div>
        </div>
        <FinalPolicyChoice
          maybeIndexes={maybeIndexes}
          onPicked={finalSelection}
          policyIndexeCrossRef={shuffledPolicySetIndexes}
          policyList={policySets[policySetIndex].partyPolicies}
          partyLabel={t('party')}
        />
      </>
    )
  }

  const getInitialChoice = () => {
    const newPartyPolicyIndex = shuffledPolicySetIndexes[partyPolicyIndex]
    return (
      <>
        <div className={classes.divide}>
          <div className={classes.constrain}>
            <p className={classes.intro}>
              {t('policySelectIntro', {
                issue: t('issue'),
                country: country.name,
                maybe: t('maybe'),
                noThanks: t('noThanks'),
              })}
            </p>
            <ProgressMarkers
              currStep={partyPolicyIndex}
              numSteps={shuffledPolicySetIndexes.length}
            />
          </div>
        </div>
        <SinglePolicyChoice
          policyList={
            policySets[policySetIndex].partyPolicies[newPartyPolicyIndex]
              .policies
          }
          partyDescription={(partyPolicyIndex + 1).toString()}
          onNoThanksClick={handleNoThanks}
          onMaybeClick={handleMaybe}
          partyLabel={t('party')}
          maybeLabel={t('maybe')}
          noThanksLabel={t('noThanks')}
        />
      </>
    )
  }

  const issueId = issuesSelected[issueSelectedIndex]
  return (
    <div className={classes.constrain}>
      <div className={classes.constrainInner}>
        <div data-ui-view='step' className={classes.content}>
          <fieldset className={classes.policy}>
            <legend className={classes.headerWrapper}>
              <span className={classes.header}>
                {t('issue')}: {issues.find(i => i.id === issueId)?.name}
              </span>
            </legend>

            {finalChoice ? getFinalChoice() : getInitialChoice()}
          </fieldset>
        </div>
      </div>
    </div>
  )
}

export default PolicySelector
