import React, { useEffect, useRef, useState } from 'react'
import CountrySelector from './countrySelector'
import Hero from './hero'
import IssueSelector from './issueSelector'
import PolicySelector from './policySelector'
import PostcodeInput from './postcodeInput'
import SurveyResults from './surveyResults'
import * as surveyApi from '../api/survey'
import classes from './survey.module.scss'
import t from '../translations'

import * as ApiTypes from 'vfp-survey-lib/types'

const surveyState = {
  SURVEY_LOADING: 0,
  ISSUE_SELECT: 1,
  COUNTRY_SELECT: 2,
  POLICY_SELECT: 3,
  POSTCODE_LOOKUP: 4,
  DISPLAY_RESULTS: 5,
}

const Survey = () => {
  const [selections, setSelections] = useState<ApiTypes.Selection[] | null>(
    null
  )
  const [selectedIssueIds, setSelectedIssueIds] = useState<number[]>([]) // This is clearly a repetition of above....
  const [selectedCountryId, setSelectedCountryId] = useState<number | null>(
    null
  )
  const [selectedPolicyIds, setSelectedPolicyIds] = useState<number[]>([])
  const [stage, setStage] = useState<number>(surveyState.SURVEY_LOADING)
  const [surveyConstituency, setSurveyConstituency] =
    useState<ApiTypes.Constituency | null>(null)
  const [surveyData, setSurveyData] = useState<{
    countries: ApiTypes.Country[]
    issues: ApiTypes.Issue[]
    parties: ApiTypes.Party[] | null
    policySets: ApiTypes.PolicySet[]
  }>({
    countries: [],
    issues: [],
    parties: null,
    policySets: [],
  })
  const [surveyResults, setSurveyResults] = useState<{
    constituency: ApiTypes.Result
    country: ApiTypes.Result
    nation: ApiTypes.Result
  } | null>(null)
  const [needScroll, setNeedScroll] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      const { data: surveyData } = await surveyApi.getCurrent()
      setSurveyData(surveyData)
      setStage(surveyState.ISSUE_SELECT)
    }

    fetchData()
  }, [])

  const handleResultsDataLoad = (resultsData: {
    constituency: ApiTypes.Constituency
    parties: ApiTypes.Party[]
    results: {
      constituency: ApiTypes.Result
      country: ApiTypes.Result
      nation: ApiTypes.Result
    }
  }) => {
    const newSurveyData = surveyData
    newSurveyData.parties = resultsData.parties

    const selections: ApiTypes.Selection[] = []

    for (let i = 0; i < selectedIssueIds.length; i++) {
      const policySet = surveyData.policySets.find(
        s => s.issueId === selectedIssueIds[i]
      )
      const partyPolicies = policySet!.partyPolicies[selectedPolicyIds[i]]
      selections.push({
        issueId: policySet!.issueId,
        partyId: partyPolicies.partyId,
      })
    }

    // Eventually this should come from the backend
    setSelections(selections)
    setSurveyConstituency(resultsData.constituency)
    setSurveyData(newSurveyData)
    setSurveyResults(resultsData.results)
    setStage(surveyState.DISPLAY_RESULTS)
  }

  const containerRef: React.Ref<HTMLDivElement> = useRef(null)
  useEffect(() => {
    if (!needScroll) {
      return
    }
    containerRef.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    })
    setNeedScroll(false)
  }, [needScroll])

  const completeIssueSelection = (issueIds: number[]) => {
    setSelectedIssueIds(issueIds)
    setStage(surveyState.COUNTRY_SELECT)
    setNeedScroll(true)
  }

  const completeCountrySelection = (countryId: number) => {
    setSelectedCountryId(countryId)
    setStage(surveyState.POLICY_SELECT)
    setNeedScroll(true)
  }

  const completePolicySelection = (policies: number[]) => {
    setSelectedPolicyIds(policies)
    setStage(surveyState.POSTCODE_LOOKUP)
    setNeedScroll(true)
  }

  const policySelected = () => {
    setNeedScroll(true)
  }

  const completePostcodeSelection = async (postcode: string) => {
    const filters: ApiTypes.Filter[] = [{ division: 2, value: postcode }]
    const { data: resultsData } = await surveyApi.submit(
      selections ?? [],
      filters
    )
    handleResultsDataLoad(resultsData)
    setNeedScroll(true)
  }

  const selectedCountry = () => {
    const index = surveyData.countries.findIndex(
      c => c.id === selectedCountryId
    )
    return surveyData.countries[index] || null
  }

  let content

  switch (stage) {
    case surveyState.SURVEY_LOADING: {
      content = <p>Loading...</p>
      break
    }
    case surveyState.ISSUE_SELECT: {
      return (
        <div className={classes.padding}>
          <Hero />
          <IssueSelector
            issueList={surveyData.issues}
            onComplete={completeIssueSelection}
          />
        </div>
      )
    }
    case surveyState.COUNTRY_SELECT: {
      content = (
        <CountrySelector
          countries={surveyData.countries}
          onComplete={completeCountrySelection}
        />
      )
      break
    }
    case surveyState.POLICY_SELECT: {
      content = (
        <PolicySelector
          country={
            selectedCountry() || {
              id: -1,
              name: 'Error',
              enabled: false,
              parties: [],
            }
          }
          issues={surveyData.issues}
          issuesSelected={selectedIssueIds}
          policySets={surveyData.policySets}
          onComplete={completePolicySelection}
          onDecision={policySelected}
        />
      )
      break
    }
    case surveyState.POSTCODE_LOOKUP: {
      content = <PostcodeInput onComplete={completePostcodeSelection} />
      break
    }
    case surveyState.DISPLAY_RESULTS: {
      const countryFind = surveyData.countries.find(
        c => c.id === selectedCountryId
      )
      const selectedCountry = countryFind || {
        id: -1,
        name: 'Error',
        enabled: false,
        parties: [],
      }
      // We force all types here as the data should be validated on load
      content = (
        <SurveyResults
          constituency={surveyConstituency as ApiTypes.Constituency}
          country={selectedCountry}
          issues={surveyData.issues}
          parties={surveyData.parties || []}
          results={
            surveyResults as {
              constituency: ApiTypes.Result
              country: ApiTypes.Result
              nation: ApiTypes.Result
            }
          }
          selections={selections ?? []}
        />
      )
      break
    }
    default:
      return null
  }

  return (
    <div className={classes.padding} ref={containerRef}>
      <div className={classes.headerContainer}>
        <div className={classes.constrainRelative}>
          <h1 className={classes.header}>
            {stage !== surveyState.DISPLAY_RESULTS
              ? t('surveyTitle')
              : t('surveyResults')}
          </h1>
        </div>
      </div>
      {content}
    </div>
  )
}

export default Survey
