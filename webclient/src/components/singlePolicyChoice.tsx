import React from 'react'
import SurveyButton from './surveyButton'
import classes from './singlePolicyChoice.module.scss'

type Props = {
  policyList: string[]
  partyDescription: string
  onNoThanksClick: () => void
  onMaybeClick: () => void
  maybeLabel: string
  noThanksLabel: string
  partyLabel: string
}

const SinglePolicyChoice = ({
  policyList,
  partyDescription,
  onNoThanksClick,
  onMaybeClick,
  maybeLabel,
  noThanksLabel,
  partyLabel,
}: Props) => {
  return (
    <div className={classes.singlePolicyChoice}>
      <div className={classes.constrain}>
        <div className={classes.headingContainer}>
          <h2 className={classes.heading}>
            {partyLabel} {partyDescription}
          </h2>
        </div>
        <ul className={classes.policyList}>
          {policyList.map((p, k) => (
            <li key={k}>{p}</li>
          ))}
        </ul>
        <div className={classes.selectPolicy}>
          <div className={classes.selectPolicyButtons}>
            <div className={classes.buttonGrid}>
              <SurveyButton
                onClick={onNoThanksClick}
                buttonLabel={noThanksLabel}
                className={classes.buttonGridButton}
                type='negative'
              />
              <SurveyButton
                onClick={onMaybeClick}
                buttonLabel={maybeLabel}
                className={classes.buttonGridButton}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SinglePolicyChoice
