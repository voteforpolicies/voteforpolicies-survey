import React from 'react'
import * as ApiTypes from 'vfp-survey-lib/types'
import { ResultsFormattedType } from '../interfaces'
import t from '../translations'
import { tabList } from '../utils/constants'
import classes from './resultsDisplay.module.scss'
import ResultsPie from './resultsPie'

type Props = {
  name?: string
  results: ResultsFormattedType
  stage: number
  surveyCount?: string
  parties: ApiTypes.Party[]
}

const ResultsDisplay = ({
  name,
  results,
  surveyCount,
  stage,
  parties,
}: Props) => {
  const totalsConstituency = (
    <>
      <strong>{`${surveyCount} ${t('results.surveysCompleted')}`}</strong>
      {t('results.inYourConstituency')}
    </>
  )
  const totals = (
    <>
      {t('results.basedOn')}
      <strong>{`${surveyCount} ${t('results.completedSurveys')}`}</strong>
    </>
  )

  let surveyTotal
  let title
  switch (stage) {
    case tabList.PERSONAL_RESULTS: {
      title = t('results.selectedPartiesHdr')
      break
    }
    case tabList.CONSTITUENCY_RESULTS: {
      title = (
        <>
          {`${t('results.resultsHdr')} `}
          <strong>{name}</strong>
        </>
      )
      surveyTotal = totalsConstituency
      break
    }
    case tabList.COUNTRY_RESULTS: {
      title = `${t('results.overallResultsHdr')} ${name}`
      surveyTotal = totals
      break
    }
    case tabList.TOTAL_RESULTS: {
      title = `${t('results.overallResultsHdr')} ${t('results.theUK')}`
      surveyTotal = totals
      break
    }
  }

  return (
    <section className={classes.resultsDisplay}>
      <div className={classes.constrain}>
        <div className={classes.titleWrapper}>
          <h1 className={classes.title}>{title}</h1>
        </div>
        <div className={classes.grid}>
          <div className={classes.gridUnit}>
            <div className={classes.chart}>
              <ResultsPie results={results} parties={parties} />
            </div>
            <div className={classes.resultsTotal}>{surveyTotal}</div>
          </div>
          <div className={classes.gridUnit}>
            <div className={classes.results}>
              {results.byParty.map((p, i) => {
                const party = parties.find(({ name }) => p.name === name)
                const partyColor = party!.color
                const partyBlockStyle = { borderLeftColor: partyColor }
                return (
                  <div
                    key={i}
                    className={`${classes.partyBlock}`}
                    style={partyBlockStyle}
                  >
                    <dl className={classes.policyParty}>
                      <dd className={classes.hGroupMain}>
                        <span className={classes.partyBlockParty}>
                          {p.name}
                        </span>
                      </dd>
                      <dt className={classes.hGroupLead}>
                        {p.count === 0
                          ? 0
                          : ((p.count / results.total) * 100).toFixed(1)}
                        %
                      </dt>
                    </dl>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ResultsDisplay
