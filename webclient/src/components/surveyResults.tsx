import classnames from 'classnames'
import React, { useState } from 'react'
import * as ApiTypes from 'vfp-survey-lib/types'
import { ResultsFormattedType } from '../interfaces'
import t from '../translations'
import { tabList } from '../utils/constants'
import PolicyChoiceList from './policyChoiceList'
import ResultsDisplay from './resultsDisplay'
import classes from './surveyResults.module.scss'

type Props = {
  country: ApiTypes.Country
  constituency: ApiTypes.Constituency
  issues: ApiTypes.Issue[]
  parties: ApiTypes.Party[]
  selections: ApiTypes.Selection[]
  results: {
    constituency: ApiTypes.Result
    country: ApiTypes.Result
    nation: ApiTypes.Result
  }
}

const SurveyResults = ({
  country,
  constituency,
  issues,
  parties,
  selections,
  results,
}: Props) => {
  const [tab, setTab] = useState<number>(tabList.PERSONAL_RESULTS)
  const [isListExpanded, setListExpanded] = useState<boolean>(false)

  const resultsFromSelection = (
    selections: ApiTypes.Selection[]
  ): ResultsFormattedType => {
    const counts: { [partyId: number]: number } = {}

    selections.forEach(s => {
      counts[s.partyId] = counts[s.partyId] ? counts[s.partyId] + 1 : 1
    })

    const byParty: { name: string; count: number }[] = []

    for (const key in counts) {
      const party = parties.find(p => p.id === Number(key))
      byParty.push({ name: String(party?.name) || 'Err', count: counts[key] })
    }

    return {
      total: selections.length,
      byParty: byParty.sort((a, b) => (a.count < b.count ? 1 : -1)),
    }
  }

  const resultsFromTotals = (
    results: { partyId: number; count: number }[]
  ): ResultsFormattedType => {
    let sum = 0

    const byParty: { name: string; count: number }[] = []

    for (const result of results) {
      sum = sum + result.count
      const party = parties.find(p => p.id === result.partyId)
      byParty.push({
        name: String(party?.name) || 'Err',
        count: result.count,
      })
    }

    return {
      total: sum,
      byParty: byParty.sort((a, b) => (a.count < b.count ? 1 : -1)),
    }
  }

  const formatCount = (n: number): string => {
    return n >= 1e4 ? (n / 1e3).toFixed(1) + 'k' : n.toString()
  }

  const onClick = (newTab: number) => {
    setTab(newTab)
    setListExpanded(!isListExpanded)
  }

  const hideTabList = () => {
    setListExpanded(false)
  }

  const tabbedState = () => {
    switch (tab) {
      case tabList.PERSONAL_RESULTS: {
        return (
          <div>
            <ResultsDisplay
              stage={tabList.PERSONAL_RESULTS}
              results={resultsFromSelection(selections)}
              parties={parties}
            />
            <p />
            <PolicyChoiceList
              issues={issues}
              parties={parties}
              policySelections={selections}
            />
          </div>
        )
      }
      case tabList.CONSTITUENCY_RESULTS: {
        return (
          <ResultsDisplay
            stage={tabList.CONSTITUENCY_RESULTS}
            name={constituency.name}
            results={resultsFromTotals(results.constituency.results)}
            surveyCount={formatCount(results.constituency.surveyCount)}
            parties={parties}
          />
        )
      }
      case tabList.COUNTRY_RESULTS: {
        return (
          <ResultsDisplay
            stage={tabList.COUNTRY_RESULTS}
            name={country.name}
            results={resultsFromTotals(results.country.results)}
            surveyCount={formatCount(results.country.surveyCount)}
            parties={parties}
          />
        )
      }
      case tabList.TOTAL_RESULTS: {
        return (
          <ResultsDisplay
            stage={tabList.TOTAL_RESULTS}
            results={resultsFromTotals(results.nation.results)}
            surveyCount={formatCount(results.nation.surveyCount)}
            parties={parties}
          />
        )
      }
    }
  }

  return (
    <>
      <div className={classes.layout}>
        <div className={classes.headerContainer}>
          <h2 className={classes.header}>{t('results.resultsIntro')}</h2>
        </div>
      </div>
      <div className={classes.resultsTabs}>
        <div className={classes.tabbing}>
          <div className={classes.constrain}>
            <ul
              role='tablist'
              className={classnames(classes.tabList, {
                [classes.tabListExpanded]: isListExpanded,
              })}
            >
              {isListExpanded && (
                <li
                  className={classnames(
                    classes.tabListItem,
                    classes.onlySmallDisplays
                  )}
                  role='tab'
                  onClick={hideTabList}
                >
                  <span className={classes.tabLinkHint}>
                    Select which results to view:
                  </span>
                </li>
              )}
              <li
                className={classes.tabListItem}
                role='tab'
                aria-selected={tab === tabList.PERSONAL_RESULTS}
                onClick={() => onClick(tabList.PERSONAL_RESULTS)}
              >
                <span className={classes.tabLink}>Personal results</span>
              </li>
              <li
                className={classes.tabListItem}
                role='tab'
                aria-selected={tab === tabList.CONSTITUENCY_RESULTS}
                onClick={() => onClick(tabList.CONSTITUENCY_RESULTS)}
              >
                <span className={classes.tabLink}>Constituency results</span>
              </li>
              <li
                className={classes.tabListItem}
                role='tab'
                aria-selected={tab === tabList.COUNTRY_RESULTS}
                onClick={() => onClick(tabList.COUNTRY_RESULTS)}
              >
                <span className={classes.tabLink}>{country.name} results</span>
              </li>
              <li
                className={classes.tabListItem}
                role='tab'
                aria-selected={tab === tabList.TOTAL_RESULTS}
                onClick={() => onClick(tabList.TOTAL_RESULTS)}
              >
                <span className={classes.tabLink}>National results</span>
              </li>
            </ul>
          </div>
        </div>
        {tabbedState()}
      </div>
    </>
  )
}

export default SurveyResults
