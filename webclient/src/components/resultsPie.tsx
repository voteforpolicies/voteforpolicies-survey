import React from 'react'
import { Cell, Pie, PieChart, ResponsiveContainer } from 'recharts'
import * as ApiTypes from 'vfp-survey-lib/types'
import { ResultsFormattedType } from '../interfaces'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { contrastColor } from 'contrast-color'

type Props = {
  results: ResultsFormattedType
  parties: ApiTypes.Party[]
}

const convertToData = (byPartyArray: ResultsFormattedType['byParty']) => {
  return byPartyArray.map((byParty: { name: string; count: number }) => {
    return { name: byParty.name, value: byParty.count }
  })
}

const createCells = (
  byPartyArray: ResultsFormattedType['byParty'],
  parties: ApiTypes.Party[]
) => {
  return byPartyArray.map((byParty: { name: string; count: number }, index) => {
    const party = parties.find(({ name }) => name === byParty.name)
    const fill = party!.color
    return <Cell key={`cell-${index}`} fill={fill} />
  })
}

const RADIAN = Math.PI / 180
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  fill,
}: {
  cx: number,
  cy: number,
  midAngle: number,
  innerRadius: number,
  outerRadius: number,
  percent: number,
  fill: string,
}): JSX.Element | null => {
  let labelHeight
  if (percent <= 0.05) {
    return null
  } else if (percent <= 0.1) {
    labelHeight = 0.65
  } else if (percent <= 0.3) {
    labelHeight = 0.6
  } else if (percent <= 0.6) {
    labelHeight = 0.5
  } else {
    labelHeight = 0.45
  }

  const radius = innerRadius + (outerRadius - innerRadius) * labelHeight
  let x = cx + radius * Math.cos(-midAngle * RADIAN)
  let y = cy + radius * Math.sin(-midAngle * RADIAN)

  if (percent === 1) {
    x = cx
    y = cy
  }

  return (
    <text
      x={x}
      y={y}
      fill={contrastColor({ bgColor: fill, threshold: 150 })}
      textAnchor='middle'
      dominantBaseline='central'
      fontFamily='lato, arial, sans-serif'
    >
      {`${(percent * 100).toFixed(1)}%`}
    </text>
  )
}

const ResultsPie = ({ results, parties }: Props) => (
  <ResponsiveContainer width='100%'>
    <PieChart>
      <Pie
        data={convertToData(results.byParty)}
        cx='50%'
        cy='50%'
        outerRadius='100%'
        startAngle={-270}
        labelLine={false}
        label={renderCustomizedLabel}
        dataKey='value'
        isAnimationActive={false}
        stroke='0px'
      >
        {createCells(results.byParty, parties)}
      </Pie>
    </PieChart>
  </ResponsiveContainer>
)

export default ResultsPie
