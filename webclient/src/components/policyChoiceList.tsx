import classnames from 'classnames'
import React from 'react'
import * as ApiTypes from 'vfp-survey-lib/types'
import classes from './policyChoiceList.module.scss'

type Props = {
  issues: ApiTypes.Issue[]
  parties: ApiTypes.Party[]
  policySelections: ApiTypes.Selection[]
}

const PolicyChoiceList = ({ issues, parties, policySelections }: Props) => {
  const choice = (selection: ApiTypes.Selection, key: number) => {
    const issue = issues.find(i => i.id === selection.issueId)!
    const party = parties.find(p => p.id === selection.partyId)!
    const color = party.color
    const blockStyle = { borderLeftColor: color }

    return (
      <div
        key={key}
        className={classnames(classes.partyBlock)}
        style={blockStyle}
      >
        <dl className={classes.policyParty}>
          <dt className={classes.issueName}>{issue.name}</dt>
          <dd className={classes.partyName}>{party.name}</dd>
        </dl>
      </div>
    )
  }

  return <div>{policySelections.map((s, i) => choice(s, i))}</div>
}

export default PolicyChoiceList
