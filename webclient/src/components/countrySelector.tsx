import React, { useState } from 'react'
import Country from './country'
import SurveyButton from './surveyButton'
import SurveyInfo from './surveyInfo'
import classes from './countrySelector.module.scss'
import t from '../translations'

import * as ApiTypes from 'vfp-survey-lib/types'

type Props = {
  countries: ApiTypes.Country[]
  onComplete: (countryId: number) => void
}

const CountrySelector = ({ countries, onComplete }: Props) => {
  const [error, setError] = useState(false)
  const [selected, setSelected] = useState<number | null>(null)

  const handleRadioClick = (countryId: number) => {
    setError(false)
    if (selected === countryId) {
      onComplete(selected)
    } else {
      setSelected(countryId)
    }
  }

  const handleButtonClick = () => {
    if (selected === null) {
      setError(true)
    } else {
      onComplete(selected)
    }
  }

  // Currently used to reset the animation as the shake only happens when the
  // error class is first applied
  const handleButtonDown = () => {
    setError(false)
  }

  return (
    <div className={classes.container}>
      <div className={classes.countrySelector}>
        <fieldset>
          <legend className={classes.legend}>
            <span className={classes.headerGroup}>
              <span className={classes.headerLead}>
                {t('showRelevantPolicies')}
              </span>
              <span className={classes.headerMain}>{t('whichCountry')}</span>
              <span className={classes.subHeader}>
                {t('nIrelandPolicies')}
                <a
                  href='https://voteforpolicies.org.uk/blog/the-2019-survey-whats-changed'
                  target='_blank'
                  rel='noreferrer'
                >
                  {t('notAvailable')}
                </a>
              </span>
            </span>
          </legend>
          <ul className={classes.countryList}>
            {countries
              .filter(c => c.enabled)
              .map(c => (
                <Country
                  key={c.id}
                  checked={c.id === selected}
                  country={c}
                  onClick={handleRadioClick}
                />
              ))}
          </ul>
          <br className={classes.clear} />
          <SurveyInfo error={error}>
            {selected ? t('proceedToPolicySelection') : t('hintDivisionOne')}
          </SurveyInfo>
          <div className={classes.formAction}>
            <SurveyButton
              icon
              onClick={handleButtonClick}
              onMouseDown={handleButtonDown}
              buttonLabel={t('next')}
              disabled={!selected}
            />
          </div>
        </fieldset>
      </div>
    </div>
  )
}

export default CountrySelector
