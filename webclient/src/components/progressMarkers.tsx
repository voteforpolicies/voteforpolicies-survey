import React from 'react'
import classes from './progressMarkers.module.scss'
import classnames from 'classnames'

type Props = {
  currStep: number
  numSteps: number
}

const ProgressMarkers = ({ currStep, numSteps }: Props) => (
  <div className={classes.progressMarker}>
    <ul className={classes.progressMarkerList}>
      {[...Array(numSteps)].map((_, k) => (
        <li
          className={classnames(classes.progressMarkerStep, {
            [classes.progressMarkerCurrent]: currStep === k,
          })}
          key={k}
        >
          {k + 1}
        </li>
      ))}
    </ul>
  </div>
)

export default ProgressMarkers
