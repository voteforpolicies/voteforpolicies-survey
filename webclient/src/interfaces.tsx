export interface ResultsFormattedType {
  total: number
  byParty: { name: string; count: number }[]
}

export interface TextEmbeddedUrlType {
  start: string
  linkText: string
  linkUrl: string
  end: string
}
