import '@testing-library/jest-dom'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import fs from 'fs'
import yaml from 'js-yaml'
import React from 'react'

const MockResponsiveContainer = props => {
  const requiredTestDimensions = { width: 400, height: 400 }
  return <div>{React.cloneElement(props.children, requiredTestDimensions)}</div>
}

jest.mock('recharts', () => ({
  ...jest.requireActual('recharts'),
  ResponsiveContainer: props => MockResponsiveContainer(props),
}))

const apiRequests = [
  {
    name: 'surveyExampleData',
    path: '/survey/current',
    type: 'get',
    code: '200',
  },
  {
    name: 'resultsExampleData',
    path: '/survey/submit',
    type: 'post',
    code: '201',
  },
]

// Convert OpenAPI yml to JSON
const file = fs.readFileSync('../lib/api.openapi.yml', 'utf8')
const doc = yaml.load(file)

// Add API spec example objects to global object...
apiRequests.forEach(req => {
  global[req.name] =
    doc.paths[req.path][req.type].responses[req.code].content[
      'application/json'
    ].example
})

// ...then use them as responses for mocked API
const handlers = apiRequests.map(({ name, path, type, code }) =>
  rest[type](`*/api/v1${path}`, (req, res, ctx) =>
    res(ctx.status(code), ctx.json(global[name]))
  )
)

// Create mock server
const server = setupServer(...handlers)
beforeAll(() => server.listen())
afterAll(() => server.close())
