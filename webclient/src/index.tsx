import React from 'react'
import ReactDOM from 'react-dom'
import Survey from './components/survey'

ReactDOM.render(
  <React.StrictMode>
    <Survey />
  </React.StrictMode>,
  document.getElementById('root')
)
