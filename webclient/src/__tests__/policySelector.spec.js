// TODO finish writing tests for this component
import React from 'react'
import { getByText, render, fireEvent, screen } from '@testing-library/react'
import PolicySelector from '../components/policySelector'
import text from '../translations/en'

describe('<PolicySelector />', () => {
  let countries,
    issues,
    policySets,
    selectedIssueId,
    onDecisionSpy,
    onCompleteSpy,
    button1,
    button2
  beforeEach(() => {
    countries = global.surveyExampleData.countries
    issues = global.surveyExampleData.issues
    policySets = global.surveyExampleData.policySets
    selectedIssueId = issues[0].id
    onDecisionSpy = jest.fn()
    onCompleteSpy = jest.fn()
    render(
      <PolicySelector
        country={countries[0]}
        issueLabel={text.issue}
        issues={issues}
        issuesSelected={[selectedIssueId]}
        maybeLabel={text.maybe}
        onDecision={onDecisionSpy}
        onComplete={onCompleteSpy}
        noThanksLabel={text.noThanks}
        policySelectIntro={text.policySelectIntro}
        policySelectNoMaybe={text.policySelectNoMaybe}
        policySelectShortlist={text.policySelectShortlist}
        policySets={policySets}
      />
    )
    button1 = screen.getAllByRole('button')[0]
    button2 = screen.getAllByRole('button')[1]
  })
  it.skip('renders issue name, intro and policies correctly', () => {
    // TODO: fix. Perhaps in conjunction with TODOs in policySelector component
    screen.getByText(`${text.issue}: ${issues[0].name}`)
    screen.getByText(
      `Here are the key ${text.issue} policies from the main parties in ${countries[0].name} (psst - we've hidden the party name!). Choose '${text.maybe}' to add to your shortlist, or '${text.noThanks}' to never see them again.`
    )
    policySets[0].partyPolicies[0].policies.forEach(p => screen.getByText(p))
    getByText(button1, text.noThanks)
    getByText(button2, text.maybe)
  })

  it('displays ooops text if no policies selected', () => {
    fireEvent.click(button1)
    // TODO use text matcher to find exact text inc link
    screen.findByText(text.policySelectNoMaybe.start)
  })

  it('displays shortlist text if single policy selected', () => {
    fireEvent.click(button2)
    // TODO use text matcher to find exact text inc link
    screen.findByText(text.policySelectShortlist.start)
  })

  it('calls onDecision when a decision is made', () => {
    fireEvent.click(button1)
    expect(onDecisionSpy).toHaveBeenCalledTimes(1)
  })

  it('calls onComplete when shortlist confirmed', () => {
    fireEvent.click(button2)
    // TODO: button should be clicked amount of times that correspond to parties with policies for this issue
    // Perhaps fix in conjunction with TODOs in policySelector component
    const fiveTimes = [...Array(5)]
    fiveTimes.forEach(() => fireEvent.click(button1))
    const nextStep = screen.getAllByRole('button')[0]
    getByText(nextStep, 'Go to next step')
    fireEvent.click(nextStep)
    expect(onCompleteSpy).toHaveBeenCalled() // i.e. called with first selection
  })
})
