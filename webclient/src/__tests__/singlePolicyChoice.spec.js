import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import SinglePolicyChoice from '../components/singlePolicyChoice'

describe('<SinglePolicyChoice />', () => {
  it('renders as expected and calls on click funcs', () => {
    const spyNoThanks = jest.fn()
    const spyMaybe = jest.fn()
    const partyLabel = 'party label'
    const partyDescription = 'party desc'
    const maybeLabel = 'maybe label'
    const noThanksLabel = 'no ta label'
    const policyList =
      global.surveyExampleData.policySets[0].partyPolicies[0].policies

    render(
      <SinglePolicyChoice
        policyList={policyList}
        partyDescription={partyDescription}
        onNoThanksClick={spyNoThanks}
        onMaybeClick={spyMaybe}
        maybeLabel={maybeLabel}
        noThanksLabel={noThanksLabel}
        partyLabel={partyLabel}
      />
    )
    screen.getByText(`${partyLabel} ${partyDescription}`)
    const button1 = screen.getAllByRole('button')[0]
    const button2 = screen.getAllByRole('button')[1]

    policyList.forEach(policy => screen.getByText(policy))

    expect(button1).toHaveTextContent(noThanksLabel)
    expect(button2).toHaveTextContent(maybeLabel)
    fireEvent.click(button1)
    expect(spyNoThanks).toHaveBeenCalled()
    fireEvent.click(button2)
    expect(spyMaybe).toHaveBeenCalled()
  })
})
