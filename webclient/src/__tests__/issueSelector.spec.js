import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import IssueSelector from '../components/issueSelector'

describe('<IssueSelector /> no issues', () => {
  it("doesn't call onComplete when button clicked if no issues present", () => {
    const spy = jest.fn()
    render(
      <IssueSelector
        issueList={[]}
        onComplete={spy}
        selectAllLabel=''
        selectAtLeastOneIssue=''
        buttonLabel=''
        surveyTimeEstimateTemplate=''
      />
    )
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    expect(spy.mock.calls.length).toEqual(0)
  })
  it('marks the hint as an error when button clicked if no issues present', () => {
    render(
      <IssueSelector
        issueList={[]}
        selectAllLabel=''
        selectAtLeastOneIssue=''
        buttonLabel=''
        surveyTimeEstimateTemplate=''
      />
    )
    screen.getByRole('note')
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    const hint = screen.queryByRole('note')
    expect(hint).toBeNull()
    screen.getByRole('alert')
  })
  it('resets the error marker when the button is pressed down', () => {
    render(
      <IssueSelector
        issueList={[]}
        selectAllLabel=''
        selectAtLeastOneIssue=''
        buttonLabel=''
        surveyTimeEstimateTemplate=''
      />
    )
    screen.getByRole('note')
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    screen.getByRole('alert')
    fireEvent.mouseDown(btn)
    const hint = screen.queryByRole('alert')
    expect(hint).toBeNull()
    screen.getByRole('note')
    fireEvent.click(btn)
    screen.getByRole('alert')
  })
})

describe('<IssueSelector /> with issues', () => {
  let spy, issues
  beforeEach(() => {
    issues = global.surveyExampleData.issues
    spy = jest.fn()
    render(
      <IssueSelector
        issueList={issues}
        onComplete={spy}
        selectAllLabel=''
        selectAtLeastOneIssue=''
        buttonLabel=''
        surveyTimeEstimateTemplate=''
      />
    )
  })
  it('displays every issue returned by api', () => {
    for (let i = 0; i < issues.length - 1; i++) {
      screen.getByText(issues[i].name)
    }
  })

  it('selects / deselects all when first checkbox checked', () => {
    const allCheckboxes = screen.getAllByRole('checkbox')
    fireEvent.click(allCheckboxes[0])
    allCheckboxes.forEach(checkbox => expect(checkbox).toBeChecked())

    fireEvent.click(allCheckboxes[0])
    allCheckboxes.forEach(checkbox => expect(checkbox).not.toBeChecked())
  })

  it('correctly calls onComplete with id of selected issue', () => {
    // First issue
    fireEvent.click(screen.getAllByRole('checkbox')[1])

    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    expect(spy).toHaveBeenCalledWith([issues[0].id])
  })

  it('resets the error marker when a country is selected', () => {
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    screen.getByRole('alert')
    fireEvent.click(screen.getAllByRole('checkbox')[1])
    const hint = screen.queryByRole('alert')
    expect(hint).toBeNull()
    screen.getByRole('note')
  })
})
