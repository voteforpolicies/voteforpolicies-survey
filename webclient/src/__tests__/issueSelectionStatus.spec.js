import React from 'react'
import { render, screen } from '@testing-library/react'
import IssueSelectionStatus from '../components/issueSelectionStatus'
import t from '../translations'

describe('<IssueSelectionStatus />', () => {
  it('displays selectAtLeastOneIssue msg', () => {
    render(<IssueSelectionStatus numSelected={0} />)
    screen.getByText(t('selectAtLeastOneIssue'))
  })

  it('displays correct selection statuses', () => {
    const { rerender } = render(<IssueSelectionStatus numSelected={1} />)
    screen.getByText('1 issue')
    screen.getByText('2-3 min')

    rerender(<IssueSelectionStatus numSelected={937} />)
    screen.getByText('937 issues')
    screen.getByText('1874-2811 min')
  })

  it('displays error styles', () => {
    const { container } = render(
      <IssueSelectionStatus numSelected={0} error={true} />
    )
    expect(container.firstChild.firstChild.classList).toContain('error')
  })
})
