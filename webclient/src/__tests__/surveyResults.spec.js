import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import SurveyResults from '../components/surveyResults'
import Language from '../translations/en'

const mockCountry = { name: 'Republic of mocking' }
const mockData = global.resultsExampleData

beforeEach(() => {
  render(
    <SurveyResults
      country={mockCountry}
      constituency={mockData.constituency}
      issues={[]}
      language={Language.results}
      parties={mockData.parties}
      selections={mockData.selections}
      results={mockData.results}
    />
  )
})

describe('<SurveyResults />', () => {
  it('displays the individual results page', () => {
    // This text only appears on the individual results page
    expect(
      screen.queryByText(Language.results.selectedPartiesHdr)
    ).not.toBeNull()
  })

  it('can switch to the constituency view', () => {
    const link = screen.getByText('Constituency results')
    fireEvent.click(link)

    const resultsHeader = screen.queryByText(Language.results.resultsHdr, {
      exact: false,
    })
    expect(resultsHeader.innerHTML).toContain(mockData.constituency.name)
  })

  it('can switch to the country view', () => {
    const link = screen.getByText(mockCountry.name + ' results')
    fireEvent.click(link)

    const resultsHeader = screen.queryByText(
      Language.results.overallResultsHdr,
      { exact: false }
    )
    expect(resultsHeader.innerHTML).toContain(mockCountry.name)
  })

  it('can switch to the overall view', () => {
    const link = screen.getByText('National results')
    fireEvent.click(link)

    const resultsHeader = screen.queryByText(
      Language.results.overallResultsHdr,
      { exact: false }
    )
    expect(resultsHeader.innerHTML).toContain('United Kingdom')
  })

  it('can switch to the overall view and back to personal', () => {
    const overallLink = screen.getByText('National results')
    fireEvent.click(overallLink)

    const personalLink = screen.getByText('Personal results')
    fireEvent.click(personalLink)

    expect(
      screen.queryByText(Language.results.selectedPartiesHdr)
    ).not.toBeNull()
  })

  it('should render the link hint when selecting a tab', () => {
    const personalLink = screen.getByRole('tab', { name: /Personal results/i })
    fireEvent.click(personalLink)

    expect(
      screen.getByText('Select which results to view:')
    ).toBeInTheDocument()
  })

  it('should remove the link hint when selecting another tab', () => {
    const personalLink = screen.getByRole('tab', { name: /Personal results/i })
    fireEvent.click(personalLink)

    const nationalLink = screen.getByRole('tab', { name: /National results/i })
    fireEvent.click(nationalLink)

    expect(
      screen.queryByText('Select which results to view:')
    ).not.toBeInTheDocument()
  })

  it('should maintain the originally selected tab when user clicks the link hint', () => {
    const originallySelectedLink = screen.getByRole('tab', { selected: true })
    fireEvent.click(originallySelectedLink)

    const hintLink = screen.getByRole('tab', {
      name: /Select which results to view:/i,
    })
    fireEvent.click(hintLink)

    expect(screen.getByRole('tab', { selected: true })).toEqual(
      originallySelectedLink
    )
  })
})
