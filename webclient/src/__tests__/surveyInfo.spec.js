import React from 'react'
import { render, screen } from '@testing-library/react'
import SurveyInfo from '../components/surveyInfo'

describe('<SurveyStatus />', () => {
  it('displays Element msg', () => {
    render(
      <SurveyInfo>
        <div>Contained Text</div>
      </SurveyInfo>
    )
    screen.getByText('Contained Text')
  })

  it('displays string msg', () => {
    render(<SurveyInfo>String Text</SurveyInfo>)
    screen.getByText('String Text')
  })

  it('displays error styles', () => {
    const { container } = render(
      <SurveyInfo error={true}>Error Status</SurveyInfo>
    )
    expect(container.firstChild.firstChild.classList).toContain('error')
  })
})
