import React from 'react'
import { fireEvent, getByText, render, screen } from '@testing-library/react'
import FinalPolicyChoice from '../components/finalPolicyChoice'

describe('<FinalPolicyChoice /> multiple choices', () => {
  const partyLabel = 'Party'
  let spy, policyList, maybeIndexes

  beforeEach(() => {
    policyList = global.surveyExampleData.policySets[0].partyPolicies
    // Add some random policy groups to the Maybe pile
    maybeIndexes = [...Array(policyList.length)].map((_, i) => i)
    const policyIndexeCrossRef = [...maybeIndexes].sort(
      () => Math.random() - 0.5
    )
    spy = jest.fn()
    render(
      <FinalPolicyChoice
        maybeIndexes={maybeIndexes}
        onPicked={spy}
        partyLabel={partyLabel}
        policyIndexeCrossRef={policyIndexeCrossRef}
        policyList={policyList}
      />
    )
  })

  it('displays header and policies', () => {
    maybeIndexes.forEach((_, i) =>
      expect(screen.getByText(`${partyLabel} ${i + 1}`)[0])
    )
    // Using getAllByText here as different parties can have identical policies
    policyList.forEach(p => p.policies.forEach(p => screen.getAllByText(p)[0]))
  })

  it('changes button text on clicking', () => {
    expect(screen.queryByText('Go to next step')).toBeNull()
    const policyButtons = screen.getAllByRole('button')
    fireEvent.click(policyButtons[0])
    getByText(policyButtons[0], 'Go to next step')
    fireEvent.click(policyButtons[1])
    getByText(policyButtons[0], 'Select these policies')
    getByText(policyButtons[1], 'Go to next step')
  })

  it('calls onPicked when same button clicked twice', () => {
    const firstPolicyButton = screen.getAllByRole('button')[0]
    fireEvent.click(firstPolicyButton)
    expect(spy.mock.calls.length).toEqual(0)
    fireEvent.click(firstPolicyButton)
    expect(spy.mock.calls.length).toEqual(1)
  })
})

describe('<FinalPolicyChoice /> single choice', () => {
  it('automatically selects policy if only one on maybe list', () => {
    render(
      <FinalPolicyChoice
        maybeIndexes={[0]}
        onPicked={jest.fn()}
        partyLabel={'abc'}
        policyIndexeCrossRef={[0]}
        policyList={global.surveyExampleData.policySets[0].partyPolicies}
      />
    )
    screen.getByText('Go to next step')
  })
})
