import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import Issue from '../components/issue'

describe('<Issue />', () => {
  it('displays the issue text', () => {
    const issue = { name: 'issue-name' }
    render(<Issue issue={issue} />)
    screen.getByLabelText('issue-name')
  })

  it('correctly checks the checkbox', () => {
    const issue = { selected: true }
    const { rerender } = render(<Issue issue={issue} />)
    expect(screen.getByRole('checkbox')).toBeChecked()

    issue.selected = false
    rerender(<Issue issue={issue} />)
    expect(screen.getByRole('checkbox')).not.toBeChecked()
  })

  it('calls onChange with correct param', () => {
    const spy = jest.fn()
    const issue = { id: 321 }
    render(<Issue issue={issue} onChange={spy} />)
    fireEvent.click(screen.getByRole('checkbox'))
    expect(spy).toHaveBeenCalledWith(321)
  })
})
