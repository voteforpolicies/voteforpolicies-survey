import { render, screen } from '@testing-library/react'
import React from 'react'
import ResultsDisplay from '../components/resultsDisplay'

describe('<ResultsDisplay />', () => {
  it('includes name on tabs other than first', () => {
    const name = 'Results title'
    const results = { total: 1, byParty: [] }
    const parties = []
    const { rerender } = render(
      <ResultsDisplay
        name={name}
        stage={1}
        results={results}
        parties={parties}
      />
    )
    expect(screen.queryByText(name)).toBeNull()
    rerender(<ResultsDisplay name={name} stage={2} results={results} />)
    screen.getByText(name)
  })

  it('renders an SVG', () => {
    const parties = []
    const { container } = render(
      <ResultsDisplay
        title='page title'
        stage={1}
        results={{ total: 1, byParty: [] }}
        parties={parties}
      />
    )

    expect(container.querySelector('svg')).not.toBeNull()
  })
})
