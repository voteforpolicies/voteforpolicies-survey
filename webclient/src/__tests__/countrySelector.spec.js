import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import CountrySelector from '../components/countrySelector'

describe('<CountrySelector />', () => {
  let countries
  beforeEach(() => {
    // TODO when mock data is fixed (i.e. deterministic) can remove this
    countries = global.surveyExampleData.countries
    countries.forEach(c => (c.enabled = true)) // eslint-disable-line no-return-assign
  })

  it('renders a radio for each country', () => {
    // TODO test country enabled
    render(<CountrySelector countries={countries} onComplete={jest.fn()} />)
    expect(screen.getAllByRole('radio')).toHaveLength(countries.length)
  })

  it('calls onComplete func if country selected when clicking btn', () => {
    const spy = jest.fn()
    const country = countries[0]
    render(<CountrySelector countries={countries} onComplete={spy} />)
    fireEvent.click(screen.getByLabelText(country.name))
    fireEvent.click(screen.getByRole('button'))
    expect(spy).toHaveBeenCalledWith(country.id)
  })

  it('calls onComplete func if country selected is clicked', () => {
    const spy = jest.fn()
    const country = countries[0]
    render(<CountrySelector countries={countries} onComplete={spy} />)
    fireEvent.click(screen.getByLabelText(country.name))
    expect(spy).not.toHaveBeenCalled()
    fireEvent.click(screen.getByLabelText(country.name))
    expect(spy).toHaveBeenCalledWith(country.id)
  })

  it('does not call onComplete func if no country selected when clicking btn', () => {
    const spy = jest.fn()
    render(<CountrySelector countries={[]} onComplete={spy} />)
    fireEvent.click(screen.getByRole('button'))
    expect(spy).not.toHaveBeenCalled()
  })

  it('marks the hint as an error when button clicked if no issues present', () => {
    render(<CountrySelector countries={[]} />)
    screen.getByRole('note')
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    const hint = screen.queryByRole('note')
    expect(hint).toBeNull()
    screen.getByRole('alert')
  })

  it('resets the error marker when the button is pressed down', () => {
    render(<CountrySelector countries={[]} />)
    screen.getByRole('note')
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    screen.getByRole('alert')
    fireEvent.mouseDown(btn)
    const hint = screen.queryByRole('alert')
    expect(hint).toBeNull()
    screen.getByRole('note')
    fireEvent.click(btn)
    screen.getByRole('alert')
  })

  it('resets the error marker when a country is selected', () => {
    const country = countries[0]
    render(<CountrySelector countries={countries} />)
    const btn = screen.getByRole('button')
    fireEvent.click(btn)
    screen.getByRole('alert')
    fireEvent.click(screen.getByLabelText(country.name))
    const hint = screen.queryByRole('alert')
    expect(hint).toBeNull()
    screen.getByRole('note')
  })
})
