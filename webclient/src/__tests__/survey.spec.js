import React from 'react'
import {
  fireEvent,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react'
import Survey from '../components/survey'
import * as surveyApi from '../api/survey'
import text from '../translations/en'

describe('<Survey /> initial states before interaction', () => {
  beforeEach(() => {
    // https://github.com/jsdom/jsdom/issues/1695
    Element.prototype.scrollIntoView = jest.fn()
  })

  const expectScrollToTop = () =>
    waitFor(() => {
      expect(Element.prototype.scrollIntoView).toHaveBeenCalledTimes(1)
      Element.prototype.scrollIntoView.mockClear()
    })

  it('displays loading text, calls api and displays issues', async () => {
    const spy = jest.spyOn(surveyApi, 'getCurrent')

    render(<Survey />)

    screen.getByText('Loading...')

    expect(spy).toHaveBeenCalled()
    await waitForElementToBeRemoved(() => screen.getByText('Loading...'))

    // Render result
    screen.getByText(text.selectAll)
    const { issues } = global.surveyExampleData
    for (let i = 0; i < issues.length - 1; i++) {
      screen.getByText(issues[i].name)
    }

    // Select all issues
    const allCheckboxes = screen.getAllByRole('checkbox')
    allCheckboxes.forEach(c => expect(c).not.toBeChecked())
    fireEvent.click(allCheckboxes[0])
    allCheckboxes.forEach(c => expect(c).toBeChecked())

    const startButton = screen.getByText(text.startSurvey)
    fireEvent.click(startButton)

    await expectScrollToTop()

    // Country selection
    const { countries } = global.surveyExampleData
    const activeCountries = countries.filter(({ enabled }) => enabled)
    activeCountries.forEach(({ name }) => screen.getByText(name))
    const firstCountry = activeCountries[0]
    const countryButton = screen.getByText(firstCountry.name)
    fireEvent.click(countryButton)
    const nextButton = screen.getByText(text.next)
    fireEvent.click(nextButton)

    await expectScrollToTop()

    // Issue selection
    const { parties: countryPartyIds } = firstCountry
    const { policySets } = global.surveyExampleData
    const isPartyPoliceForCountry = ({ partyId }) =>
      countryPartyIds.includes(partyId)
    const countryIssues = policySets.filter(({ partyPolicies }) =>
      partyPolicies.find(isPartyPoliceForCountry)
    )
    for (const countryIssue of countryIssues) {
      const partyPolicies = countryIssue.partyPolicies.filter(
        isPartyPoliceForCountry
      )
      for (let i = 0; i < partyPolicies.length; i++) {
        const maybeButton = screen.getByText(text.maybe)
        fireEvent.click(maybeButton)
        await expectScrollToTop()
      }
      if (partyPolicies.length > 1) {
        const button = screen.getAllByText('Select these policies')[0]
        fireEvent.click(button)
      }
      const nextButton = screen.getByText('Go to next step')
      fireEvent.click(nextButton)
    }

    await expectScrollToTop()

    // Postcode entry & submit
    const postCodeInput = screen.getByRole('textbox')
    fireEvent.change(postCodeInput, { target: { value: 'A' } })
    const submitButton = screen.getByText(text.surveySubmit)
    fireEvent.click(submitButton)

    await expectScrollToTop()
  })
})
