import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import Country from '../components/country'

describe('<Country />', () => {
  it('correctly renders country name', () => {
    render(<Country country={{ name: 'eSwatini' }} />)
    screen.getByLabelText('eSwatini')
  })

  it('correctly renders when checked', () => {
    render(<Country checked={true} country={{ name: 'South Sudan' }} />)
    expect(screen.getByRole('radio')).toBeChecked()
  })

  it('correctly renders when not checked', () => {
    render(<Country country={{ name: 'Guinea Bissau' }} />)
    expect(screen.getByRole('radio')).not.toBeChecked()
  })

  it('calls onclick method with correct param when selected', () => {
    const spy = jest.fn()
    render(<Country country={{ id: 123, name: 'Burundi' }} onClick={spy} />)
    fireEvent.click(screen.getByRole('radio'))
    expect(spy).toHaveBeenCalledWith(123)
  })
})
