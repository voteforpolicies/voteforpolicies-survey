import React from 'react'
import { render, screen } from '@testing-library/react'
import ProgressMarkers from '../components/progressMarkers'

describe('<ProgressMarkers />', () => {
  it('correctly indicates all steps and current step number', () => {
    for (let numSteps = 1; numSteps < 10; numSteps++) {
      // currStep is in a range from 0 -> numSteps-1
      for (let currStep = 0; currStep < numSteps; currStep++) {
        const { unmount } = render(
          <ProgressMarkers numSteps={numSteps} currStep={currStep} />
        )
        for (let i = 1; i < numSteps; i++) {
          if (i === currStep + 1) {
            expect(screen.getByText(`${i}`)).toHaveClass(
              'progressMarkerCurrent'
            )
          } else {
            expect(screen.getByText(`${i}`)).not.toHaveClass(
              'progressMarkerCurrent'
            )
          }
        }
        unmount()
      }
    }
  })
})
