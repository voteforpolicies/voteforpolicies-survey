import * as functions from 'firebase-functions'
import * as Express from 'express'
// eslint-disable-next-line import/no-unresolved,node/no-missing-import
import makeApp from './api/app'

const funcConfig: functions.config.Config = functions.config()
const funcConfigApi: any = (funcConfig.api || {})
const cors: string[] = funcConfigApi.cors ?? []

const webApi: Express.Application = makeApp({
  ...(funcConfigApi.cacheType && {cacheType: funcConfigApi.cacheType}),
  cors
})

export const apiFunc: functions.HttpsFunction = functions.https.onRequest(
    webApi
)
