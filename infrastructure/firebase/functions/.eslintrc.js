module.exports = {
  env: {
    es6: true,
    'jest/globals': true
  },
  'extends': [
    'plugin:import/recommended',
    'plugin:jest/recommended', 
    'plugin:node/recommended', 
    'plugin:promise/recommended', 
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'comma-dangle': 0,
    'no-unused-vars': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-unused-vars': 'error',
    'node/no-unsupported-features/es-syntax': ['error', { ignores: ['modules'] }],
    'node/no-unpublished-import': 0,
    'node/no-missing-import': ['error', {
      'allowModules': ['vfp-survey-lib']
    }]
  },
  settings: {
    'import/resolver': {
      typescript: {} // this loads <rootdir>/tsconfig.json to eslint
    },
    node: {
      tryExtensions: ['.js', '.json', '.ts'],
    }
  },
  ignorePatterns: [
    "/lib/**/*", // Ignore built files.
  ],
}
