#!/usr/bin/env bash

docker-compose run --rm webbuild ./node_modules/.bin/eslint 'src/*.tsx' 'src/**/*.tsx'
