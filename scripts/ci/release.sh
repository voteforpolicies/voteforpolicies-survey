#!/usr/bin/env bash

set -e

REPO_BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && cd ../.. && pwd )"

build_webclient() {
  cd $1
  npm run build
}

copy_webclient_files() {
  SRC=$1
  DST=$2

  rm -rf $DST
  mkdir -p $DST/static/css $DST/static/js
  cp $SRC/index.html $DST
  cp $SRC/static/css/*.css $DST/static/css/
  cp $SRC/static/js/*.js{,.LICENSE.txt} $DST/static/js/
  cp -r $SRC/static/media/ $DST/static/media/
}

firebase_release() {
  CLIENT_DIR=$REPO_BASE_DIR/webclient
  RELEASE_DIR=$REPO_BASE_DIR/infrastructure/firebase

  build_webclient $CLIENT_DIR
  copy_webclient_files $CLIENT_DIR/build $RELEASE_DIR/public

  cd $RELEASE_DIR
  firebase deploy
}

case $1 in 
  firebase)
    firebase_release
  ;;
  *)
    echo "I only know how to deploy to Firebase at the moment :'("
    exit 1
  ;;
esac
