#!/usr/bin/env bash

BUILD_DIR="build/webclient"

mkdir -p $BUILD_DIR
rm -rf $BUILD_DIR/*

docker-compose run webbuild
