import { components } from './api'

type ConstituencyType = components['schemas']['Constituency']
type CountryType = components['schemas']['Country']
type ErrorType = components['schemas']['Error']
type FilterType = components['schemas']['Filter']
type IssueType = components['schemas']['Issue']
type PartyType = components['schemas']['Party']
type PolicySetType = components['schemas']['PolicySet']
type PartyPolicyType = components['schemas']['PartyPolicy']
type SelectionType = components['schemas']['Selection']
type SurveyAnonType = components['schemas']['SurveyAnon']
type ResultType = components['schemas']['Result']
type ResultsPersonalType = components['schemas']['ResultsPersonal']

declare namespace api {
  interface Constituency extends ConstituencyType {}
  interface Country extends CountryType {}
  interface Error extends ErrorType {}
  interface Filter extends FilterType {}
  interface Issue extends IssueType {}
  interface Party extends PartyType {}
  interface PolicySet extends PolicySetType {}
  interface PartyPolicy extends PartyPolicyType {}
  interface Selection extends SelectionType {}
  interface SurveyAnon extends SurveyAnonType {}
  interface Result extends ResultType {}
  interface ResultsPersonal extends ResultsPersonalType {}
}

export = api